﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xf="http://www.w3.org/2002/xforms"
  xmlns:ev="http://www.w3.org/2001/xml-events"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes"/>

  <xsl:variable name="dataset-name" select="/xs:schema/xs:element[@msdata:IsDataSet='true']/@name"/>
  
  <xsl:template match="/xs:schema">
    <xsl:processing-instruction name="xml-stylesheet">
    href="xsltforms/xsltforms.xsl" type="text/xsl"
    </xsl:processing-instruction>
    <xsl:processing-instruction name="xsltforms-options">
    debug=yes   
    </xsl:processing-instruction>
    <html>
      <!-- copy the mstns namespace -->
      <xsl:copy-of select="/xs:schema/namespace::*[name()='mstns']"/>
      <head>
        <title>
          <xsl:text>Generic editor for dataset: </xsl:text>
          <xsl:value-of select="$dataset-name"/>
        </title>
        <!-- CSS goes in the document HEAD or added to your external stylesheet -->
        <style type="text/css">
        table.gridtable {
	        font-family: verdana,arial,sans-serif;
	        font-size:11px;
	        color:#333333;
	        border-width: 1px;
	        border-color: #666666;
	        border-collapse: collapse;
        }
        table.gridtable th {
	        border-width: 1px;
	        padding: 8px;
	        border-style: solid;
	        border-color: #666666;
	        background-color: #dedede;
        }
        table.gridtable td {
	        border-width: 1px;
	        padding: 8px;
	        border-style: solid;
	        border-color: #666666;
	        background-color: #ffffff;
        }
        </style>
        <script type="text/javascript"><![CDATA[
function DS2XF_SortDataTable(instanceID) {
  var instanceElement = document.getElementById(instanceID);
  if (instanceElement!=null) {
	  // XForms exposes the retrival of the instance document from the model element which *should*
	  // be the parent for the instance element.
    var instance = instanceElement.parentNode.getInstanceDocument(instanceID);
		if (instance!=null) {
		  // Now creating the stylesheet, for this example the stylesheet document is also an instance
			// by it can be loaded from many difference sources
			var xslDom = instanceElement.parentNode.getInstanceDocument('sorting');
			
			// create an XSLTProcessor and attach the stylesheet
		  var processor = new XSLTProcessor();
			processor.importStylesheet(xslDom);
			
			// now we do the sorting transformation
			var resultDom = processor.transformToDocument(instance, instance);
			
			// we then move the result info the instance dom
			instance.removeChild(instance.documentElement);
			instance.appendChild(resultDom.documentElement);
			
			// and performs the updates for XForms
			instanceElement.parentNode.rebuild();
			instanceElement.parentNode.recalculate();
			instanceElement.parentNode.revalidate();
			instanceElement.parentNode.refresh();
		}
	}
}
        ]]></script>
        <xsl:call-template name="Model"/>
      </head>
      <body>
        <xsl:apply-templates select="xs:element[@msdata:IsDataSet='true']" mode="DataSet2Input"/>
      </body>
    </html>
  </xsl:template>
  
  <!-- binds -->
  <xsl:template match="xs:element" mode="DataSet2Binds">
    <xsl:variable name="root-path" select="concat('/diffgr:diffgram/mstns:', @name)"/>
    <xsl:apply-templates select="xs:complexType/xs:choice/xs:element" mode="DataTable2Binds">
      <xsl:with-param name="path" select="$root-path"/>    
    </xsl:apply-templates>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataTable2Binds">
    <xsl:param name="path"/>
    <xsl:variable name="table-path" select="concat($path, concat('/mstns:', @name))"/>
    <xf:bind>
      <xsl:attribute name="nodeset">
        <xsl:value-of select="$table-path"/>
      </xsl:attribute>
      <!-- irrelevant data is not sent!
      <xsl:attribute name="relevant">
        <xsl:text>@diffgr:hasChanges</xsl:text>
      </xsl:attribute>-->
    </xf:bind>
    <xsl:for-each
        select="xs:complexType/xs:sequence/xs:element">
      <xf:bind>
        <xsl:attribute name="nodeset">
          <xsl:value-of select="$table-path"/>
          <xsl:text>/mstns:</xsl:text>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
        <xsl:attribute name="type">
          <xsl:value-of select="@type"/>
        </xsl:attribute>
        <xsl:attribute name="required">
          <xsl:choose>
            <xsl:when test="@minOccurs = '0'">
              <xsl:text>false()</xsl:text>            
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>true()</xsl:text>
            </xsl:otherwise>
          </xsl:choose>        
        </xsl:attribute>
      </xf:bind>
    </xsl:for-each>
  </xsl:template>
  
  <!-- default values -->
  <xsl:template match="xs:element" mode="DataSet2DefaultInstance">
    <xsl:element name="{@name}" namespace="{/xs:schema/@targetNamespace}">
      <xsl:apply-templates select="xs:complexType/xs:choice/xs:element" mode="DataTable2DefaultInstance"/>
    </xsl:element>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataTable2DefaultInstance">
    <xsl:element name="{@name}" namespace="{/xs:schema/@targetNamespace}">
      <xsl:attribute name="diffgr:hasChanges">
        <xsl:text>added</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="diffgr:id">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="msdata:rowOrder">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
      <xsl:for-each
          select="xs:complexType/xs:sequence/xs:element">
        <xsl:element name="{@name}" namespace="{/xs:schema/@targetNamespace}">
          <xsl:choose>
            <xsl:when test="@default">
              <xsl:value-of select="@default"/>
            </xsl:when>          
          </xsl:choose>
          <!-- otherwise, blank -->
        </xsl:element>
      </xsl:for-each>    
    </xsl:element>
  </xsl:template>  
  
  <!-- autoinc columns -->
  <xsl:template match="xs:element" mode="DataSet2AutoIncInstance">
    <xsl:element name="{@name}" namespace="{/xs:schema/@targetNamespace}">
      <xsl:apply-templates
        select="xs:complexType/xs:choice/xs:element"
        mode="DataTable2AutoIncInstance"/>
    </xsl:element>
  </xsl:template>
  <xsl:template match="xs:element[xs:complexType/xs:sequence/xs:element/@msdata:AutoIncrement='true']" mode="DataTable2AutoIncInstance">
    <xsl:element name="{@name}" namespace="{/xs:schema/@targetNamespace}">
      <xsl:for-each
          select="xs:complexType/xs:sequence/xs:element[@msdata:AutoIncrement='true']">
        <xsl:element name="{@name}" namespace="{/xs:schema/@targetNamespace}">
          <xsl:copy-of select="@msdata:AutoIncrementSeed | @msdata:AutoIncrementStep"/>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <!-- controls -->
  <xsl:template match="xs:element" mode="DataSet2Input">
    <xf:group>
      <xsl:attribute name="ref">
        <!-- TODO: how to output a dynamic value here? with the statically known ns? -->
        <xsl:text>/diffgr:diffgram</xsl:text>
        <xsl:text>/mstns:</xsl:text>
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xf:label>
        <xsl:text>dataset </xsl:text>
        <strong><xsl:value-of select="@name"/></strong>
      </xf:label>
      <xsl:apply-templates select="." mode="DataSet2SaveTrigger">
        <xsl:with-param name="dataset-name" select="@name"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="." mode="DataSet2ReloadTrigger">
        <xsl:with-param name="dataset-name" select="@name"/>
      </xsl:apply-templates>
      <br />
      <xsl:apply-templates select="xs:complexType/xs:choice/xs:element" mode="DataTable2Input"/>
    </xf:group>     
  </xsl:template>
  <xsl:template match="xs:element" mode="DataTable2Input">
    <xsl:variable name="table-name" select="@name"/>
    <xsl:variable name="fq-table-elem" select="concat('mstns:', $table-name)"/>
    <xsl:variable name="repeat-name" select="concat('repeat-', $table-name)"/>
    <!--
      NOTE: this is only for inline-editable tables,
      but sometimes we want separate editing forms -->
    <table class="gridtable">
      <thead>
        <tr>
          <xsl:apply-templates select="xs:complexType/xs:sequence/xs:element" mode="DataColumn2ColumnTitle">
            <xsl:with-param name="tableName" select="$table-name"/>
          </xsl:apply-templates>
        </tr>
      </thead>
      <tbody>
        <xf:repeat id="{$repeat-name}">
          <xsl:attribute name="ref">
            <xsl:value-of select="$fq-table-elem"/>
            <!-- do not display deleted elements -->
            <xsl:text>[not(@diffgr:hasChanges = 'deleted')]</xsl:text>
          </xsl:attribute>
          <tr>
            <xsl:apply-templates select="xs:complexType/xs:sequence/xs:element" mode="DataColumn2Input">
              <xsl:with-param name="dataset-name" select="$dataset-name"/>              
              <xsl:with-param name="table-name" select="@name"/>
              <xsl:with-param name="repeat-name" select="$repeat-name"/>
            </xsl:apply-templates>
          </tr>
        </xf:repeat>
      </tbody>
     </table>
     <xsl:apply-templates select="." mode="DataTable2EditTrigger">
        <xsl:with-param name="dataset-name" select="$dataset-name"/>
        <xsl:with-param name="table-name" select="$table-name"/>
        <xsl:with-param name="repeat-name" select="$repeat-name"/>
     </xsl:apply-templates>
     <xsl:apply-templates select="." mode="DataTable2AddTrigger">
       <xsl:with-param name="dataset-name" select="$dataset-name"/>
       <xsl:with-param name="table-name" select="$table-name"/>
       <xsl:with-param name="repeat-name" select="$repeat-name"/>
     </xsl:apply-templates>
     <xsl:apply-templates select="." mode="DataTable2DeleteTrigger">
       <xsl:with-param name="dataset-name" select="$dataset-name"/>
       <xsl:with-param name="table-name" select="$table-name"/>
       <xsl:with-param name="repeat-name" select="$repeat-name"/>
     </xsl:apply-templates>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataRow2DiffgramID">
    <xsl:param name="dataset-name"/>
    <xsl:param name="table-name"/>
    <xsl:param name="item-path"/>
    
    <!-- regardless of any autoinc columns in the table... rely on the already calculated rowOrder -->
    <xf:setvalue ref="{$item-path}/@diffgr:id">
      <xsl:attribute name="value">
        <xsl:text>concat('</xsl:text>
        <xsl:value-of select="$table-name"/>
        <xsl:text>', string(</xsl:text>
        <xsl:value-of select="concat($item-path, '/@msdata:rowOrder')"/>
        <xsl:text>+ 1)</xsl:text> <!-- string -->
        <xsl:text>)</xsl:text> <!-- concat -->
      </xsl:attribute>
    </xf:setvalue>    
    <!-- can we even obtain the key at this point? NOPE...
    and that's because we don't have any values...
    we could have, only if the key has an autoincrement field
    which should automatically fire
    <xf:setvalue ref="{$item-path}/@diffgr:id">
    </xf:setvalue>
    <xsl:apply-templates select="xs:complexType/xs:sequence/xs:element" mode="DataColumn2Input">
      <xsl:with-param name="tableName" select="@name"/>
    </xsl:apply-templates>
    -->
  </xsl:template>
  <!-- TODO: remove the below template, unused -->
  <xsl:template match="xs:element" mode="DataTable2EditTrigger">
    <xsl:param name="dataset-name"/>
    <xsl:param name="table-name"/>
    <xsl:param name="repeat-name"/>
    <xsl:variable
      name="item-path"
      select="
        concat('/diffgr:diffgram/mstns:', $dataset-name,
          '/mstns:', @name, '[last()]')"/>
    
  </xsl:template>
  <xsl:template match="xs:element" mode="DataTable2AddTrigger">
    <xsl:param name="dataset-name"/>
    <xsl:param name="table-name"/>
    <xsl:param name="repeat-name"/>

    <xsl:variable name="fq-table-elem" select="concat('mstns:', $table-name)"/>
    
    <xf:trigger>
      <xf:label><img src="images/Image.Add.png"/> Add <xsl:value-of select="$table-name"/></xf:label>
      <xsl:variable
        name="item-path"
        select="
          concat('/diffgr:diffgram/mstns:', $dataset-name,
            '/mstns:', @name, '[last()]')"/>
        
      <xf:action ev:event="DOMActivate">
        <xf:insert position="after" at="last()" ref="{$fq-table-elem}">
          <xsl:attribute name="origin">
            <xsl:text>instance('default-dataset')</xsl:text>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$fq-table-elem"/>
          </xsl:attribute>
        </xf:insert>
        <xf:setvalue ref="{$item-path}/@msdata:rowOrder">
          <!-- NOTE: only if there exist the previous element... otherwise leave it at 0 -->
          <xsl:attribute name="if">
            <xsl:value-of select="concat('/diffgr:diffgram/mstns:', $dataset-name, '/', $fq-table-elem)"/>
            <xsl:text>[last()-1]</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="concat('/diffgr:diffgram/mstns:', $dataset-name, '/', $fq-table-elem)"/>
            <xsl:text>[last()-1]/@msdata:rowOrder + 1</xsl:text>
          </xsl:attribute>
        </xf:setvalue>
        <xsl:apply-templates select="." mode="DataRow2DiffgramID">
          <xsl:with-param name="dataset-name" select="$dataset-name"/>
          <xsl:with-param name="table-name" select="$table-name"/>
          <xsl:with-param name="item-path" select="$item-path"/>
        </xsl:apply-templates>
        <!-- emit autoincrement actions, if any -->
        <xsl:apply-templates select="xs:complexType/xs:sequence/xs:element[@msdata:AutoIncrement='true']" mode="DataColumn2AutoInc">
          <xsl:with-param name="item-path" select="$item-path"/>        
          <xsl:with-param name="dataset-name" select="$dataset-name"/>
          <xsl:with-param name="table-name" select="$table-name"/>
        </xsl:apply-templates>
        <!-- TODO: put the cursor in the first column of the grid
        currently, cannot focus the needed row
        NOTE: this also conflicts with the first column being read-only (e.g. an xf:output)
        <xf:setfocus>
          <xsl:attribute name="control">
            <xsl:value-of select="$table-name"/>
            <xsl:text>-</xsl:text>
            <xsl:value-of select="xs:complexType/xs:sequence/xs:element[1]/@name"/>
          </xsl:attribute>          
        </xf:setfocus>            -->
      </xf:action>
    </xf:trigger>    
  </xsl:template>
  <xsl:template match="xs:element" mode="DataColumn2AutoInc">
    <xsl:param name="item-path"/>
    <xsl:param name="dataset-name"/>
    <xsl:param name="table-name"/>
    
    <!-- obtain seed and step -->
    <xsl:variable name="autoinc-column-path">
      <xsl:text>instance('autoinc-dataset')</xsl:text>
      <xsl:text>/mstns:</xsl:text>
      <xsl:value-of select="$table-name"/>
      <xsl:text>/mstns:</xsl:text>
      <xsl:value-of select="@name"/>
    </xsl:variable>

    <!--
    <xf:message>autoinc <xf:output value="serialize({$autoinc-column-path}/@msdata:AutoIncrementSeed)"/></xf:message>
      -->
    <xf:setvalue
      ref="{$item-path}/mstns:{@name}"
      value="{$autoinc-column-path}/@msdata:AutoIncrementSeed + {$autoinc-column-path}/@msdata:AutoIncrementStep"/>
    <!--
    <xf:message>before: <xf:output value="serialize({$autoinc-column-path})"/></xf:message>
    -->
    <xf:setvalue
      ref="{$autoinc-column-path}/@msdata:AutoIncrementSeed"
      value="{$autoinc-column-path}/@msdata:AutoIncrementSeed + {$autoinc-column-path}/@msdata:AutoIncrementStep"/>
    <!--
    <xf:message>after: <xf:output value="serialize({$autoinc-column-path})"/></xf:message>-->
  </xsl:template>
  <xsl:template match="xs:element" mode="DataTable2DeleteTrigger">
    <xsl:param name="dataset-name"/>
    <xsl:param name="table-name"/>
    <xsl:param name="repeat-name"/>
    <xf:trigger>
      <!-- TODO: add a generic confirmation dialog https://en.wikibooks.org/wiki/XForms/Delete_Confirm -->
      <xf:label><img src="images/Image.Delete.png"/> Delete <xsl:value-of select="@name"/></xf:label>
      <xsl:variable
        name="item-path"
        select="
          concat('/diffgr:diffgram/mstns:', $dataset-name,
            '/mstns:', @name,
            '[index(&quot;', $repeat-name, '&quot;)]')"/>
      <xf:action ev:event="DOMActivate" if="{$item-path}[@diffgr:hasChanges='added']">
        <!-- discard the element completely
        I hope we don't have to recalculate any other values?
        e.g. if this is a parent... then delete children?
        -->
        <xf:message>added!</xf:message>
        <xf:delete nodeset="{$item-path}"/>
      </xf:action>
      <xf:action ev:event="DOMActivate" if="{$item-path}[not(@diffgr:hasChanges) or @diffgr:hasChanges='modified']">
        <!-- if the row is unmodified... -->
        <xf:insert
          context="/diffgr:diffgram/diffgr:before" 
          nodeset="*"
          origin="{$item-path}"
          if="{$item-path}[not(@diffgr:hasChanges)]"/>
        <!-- if has changes already, or if unmodified -->
        <xf:delete nodeset="{$item-path}"/>
      </xf:action>
    </xf:trigger>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataSet2ReloadTrigger">
    <xsl:param name="dataset-name"/>
    <xf:dialog id="confirm-fetch-instance">
      <p>Are you sure you want to reload? You have unsaved changes.</p>
      <xf:trigger>
        <xf:label>Yes</xf:label>
        <xf:action ev:event="DOMActivate">
          <xf:hide dialog="confirm-fetch-instance"/>
          <xf:send submission="fetch-instance"/>
        </xf:action>
      </xf:trigger>
      <xf:trigger>
        <xf:label>No</xf:label>
        <xf:hide dialog="confirm-fetch-instance" ev:event="DOMActivate"/>
      </xf:trigger>
    </xf:dialog>
    <xf:trigger>
      <xf:label><img src="/images/Image.Reject.png"/> Reload</xf:label>
      <xf:hint>Reject your changes and reload with fresh data.</xf:hint>
      <!-- show a confirmation dialog if there are changes -->
      <xf:show dialog="confirm-fetch-instance" ev:event="DOMActivate">
        <xsl:attribute name="if">
          <xsl:text>.[count(*[@diffgr:hasChanges]) > 0]</xsl:text>
        </xsl:attribute>
      </xf:show>
      <!-- just submit if there are no changes -->
      <xf:send submission="fetch-instance" ev:event="DOMActivate">
        <xsl:attribute name="if">
          <xsl:text>.</xsl:text>
          <xsl:text>[not(*[@diffgr:hasChanges])]</xsl:text>
        </xsl:attribute>
      </xf:send>
    </xf:trigger>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataSet2SaveTrigger">
    <xsl:param name="dataset-name"/>
    <!-- don't allow saving unless there are changes -->
    <xf:trigger ref=".[count(*[@diffgr:hasChanges]) &gt; 0 or count(/diffgr:diffgram/diffgr:before/*) &gt; 0]">
      <xf:label><img src="/images/Image.Accept.png" /> Save Changes</xf:label>
      <xf:hint>Accept your changes and save them.</xf:hint>
      <xf:action ev:event="DOMActivate">
        <!-- prepare the changes -->
        <xf:insert
          origin="instance('instance-{$dataset-name}')"
          nodeset="instance('instance-{$dataset-name}-changes')"/>
        <xf:delete
          nodeset="instance('instance-{$dataset-name}-changes')/mstns:{$dataset-name}/*[not(@diffgr:hasChanges)]"/>
        <!--
        <xf:message>
          <xf:output value="serialize(instance('instance-{$dataset-name}-changes')/mstns:{$dataset-name})"/>
        </xf:message>-->
        <xf:send submission="save-instance"/>
      </xf:action>
    </xf:trigger>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataColumn2ColumnTitle">
    <xsl:param name="tableName"/>
    <td>
      <xsl:choose>
        <xsl:when test="@msdata:Caption">
          <xsl:value-of select="@msdata:Caption"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@name"/>          
        </xsl:otherwise>
      </xsl:choose>
    </td>
  </xsl:template>
  <xsl:template match="xs:element" mode="DataColumn2Input">
    <xsl:param name="dataset-name"/>
    <xsl:param name="table-name"/>
    <xsl:param name="repeat-name"/>
    <xsl:variable
      name="item-path"
      select="
        concat('/diffgr:diffgram/mstns:', $dataset-name,
          '/mstns:', $table-name,
          '[index(&quot;', $repeat-name, '&quot;)]')"/>
    <td>
      <xsl:variable name="control-id" select="concat($table-name, '-', @name)"/>
      <xsl:variable name="control-ref" select="concat('mstns:', @name)"/>
      <!-- if this is read-only control, and it doesn't have @diffgr:hasChanges='added', it is mapped to readonly -->
      <xsl:choose>
        <xsl:when test="@msdata:AutoIncrement='true'">
          <!-- never allow editing auto increment fields, how is that even supposed to work? -->
          <xf:output id="{$control-id}-out" value="{$control-ref}"/>
        </xsl:when>
        <xsl:when test="@msdata:ReadOnly='true'">
          <xf:group ref="{$control-ref}[../@diffgr:hasChanges = 'added']">
            <xf:input id="{$control-id}-in" ref=".">
              <xsl:apply-templates select="." mode="DataColumn2InputChangeTracking">
                <xsl:with-param name="item-name" select="$table-name"/>
                <xsl:with-param name="item-path" select="$item-path"/>
                <xsl:with-param name="control-id" select="concat($control-id, '-in')"/>
                <xsl:with-param name="column-path" select="concat($item-path, '/mstns:', @name)"/>
              </xsl:apply-templates>
            </xf:input>
          </xf:group>
          <xf:group ref="{$control-ref}[not(../@diffgr:hasChanges) or ../@diffgr:hasChanges != 'added']">
            <xf:output id="{$control-id}-out" value="."/>
          </xf:group>
        </xsl:when>
        <xsl:otherwise>
          <xf:input id="{$control-id}-in" ref="{$control-ref}">
            <xsl:apply-templates select="." mode="DataColumn2InputChangeTracking">
              <xsl:with-param name="item-name" select="$table-name"/>
              <xsl:with-param name="item-path" select="$item-path"/>
              <xsl:with-param name="control-id" select="concat($control-id, '-in')"/>
              <xsl:with-param name="column-path" select="concat($item-path, '/mstns:', @name)"/>
            </xsl:apply-templates>
          </xf:input>
        </xsl:otherwise>
      </xsl:choose>
    </td>  
  </xsl:template>
  <xsl:template match="xs:element" mode="DataColumn2InputChangeTracking">
    <xsl:param name="item-path"/>
    <xsl:param name="control-id"/>
    <xsl:param name="item-name"/>
    <xsl:param name="column-path"/>
    <xsl:variable
      name="item-path-before"
      select="concat('/diffgr:diffgram/diffgr:before/mstns:', $item-name, '[@diffgr:id = ', $item-path, '/@diffgr:id]')"/>
    
    <!-- idea: only write to diffgr:before on a confirmed change -->
    <xf:action ev:event="DOMFocusIn" if="not({$item-path}/@diffgr:hasChanges)">
      <!-- replace the old value -->
      <xf:insert
        ref="instance('diff-old-value')"
        origin="{$item-path}"/>
      <!--
      <xf:message level="modeless">after focusing in: <xf:output value="serialize(instance('diff-old-value'))"/></xf:message>
      <xf:message level="modeless">before element: <xf:output value="serialize(/diffgr:diffgram/diffgr:before)"/></xf:message>
      -->
    </xf:action>
    <!--
    NOTE: the event sequence seems to be:
    DOMFocusIn, DOMFocusOut, xforms-value-changed
    but it should be the other way round, http://www.w3.org/TR/2003/REC-xforms-20031014/slice4.html#evt-refresh
    
    while we would seriously prefer it to be:
    DOMFocusIn, xforms-value-changed, DOMFocusOut
      -->    
    <!-- initially the row was unmodified, but now something changed -->
    <xf:action ev:event="xforms-value-changed" if="not({$item-path}/@diffgr:hasChanges)">
      <!-- copy the old value, OR replace it?? -->
      <xf:insert
        context="/diffgr:diffgram/diffgr:before"
        origin="instance('diff-old-value')"/>
      <!-- set modified flag -->
      <xf:insert
        origin="instance('diffgram-templates')/modified/@diffgr:hasChanges"
        context="{$item-path}"/>
      <!--
      <xf:message>modified flag set: <xf:output value="serialize({$item-path})"/></xf:message>
      <xf:message>saved to diffgr:before: <xf:output value="serialize({$item-path-before})"/></xf:message>
      -->
    </xf:action>
  </xsl:template>
  
  <xsl:template name="Model">
    <xf:model>
      <!-- NOTE: just a resource... we need descriptive documents for hyperlinking
      also, DO NOT move this instance (it's the default instance)
      -->
      <xf:instance id="instance-{$dataset-name}" src="/services/{$dataset-name}Service.aspx"/>
      <xf:instance id="instance-{$dataset-name}-changes">
        <diffgr:diffgram/>
      </xf:instance>
      <xsl:apply-templates select="xs:element[@msdata:IsDataSet='true']" mode="DataSet2Binds"/>
      <xf:instance id="default-dataset">
        <xsl:apply-templates select="xs:element[@msdata:IsDataSet='true']" mode="DataSet2DefaultInstance"/>
      </xf:instance>
      <xf:instance id="autoinc-dataset">
        <xsl:apply-templates select="xs:element[@msdata:IsDataSet='true']" mode="DataSet2AutoIncInstance"/>
      </xf:instance>
      <xf:instance id="diffgram-templates">
        <data xmlns="">
          <deleted diffgr:hasChanges="deleted"/>
          <modified diffgr:hasChanges="modified"/>
          <diffgr:before/>
        </data>
      </xf:instance>
      <xf:instance id="diff-old-value">
        <data xmlns="">
          <diffgr:before/>            
        </data>
      </xf:instance>
      <xf:instance id="sorting" src="/sorting.xsl"/>
      <xf:action ev:event="xforms-ready">
        <!-- insert this element as soon as possible, it will be needed for change-tracking
        NOTE: we must also insert this element AFTER replacing old contents with the new contents...
        -->
        <xf:insert
          nodeset="/diffgr:diffgram/mstns:{$dataset-name}"
          origin="instance('diffgram-templates')/diffgr:before">
        </xf:insert>
      </xf:action>
      <xsl:call-template name="FetchInstanceAndReplaceDiffgram"/>
      <xsl:call-template name="SaveInstanceAndMergeDiffgrams"/>
      <!--
      DEBUG: show what you are trying to submit
      <xf:message ev:event="xforms-submit" level="modal">Submitting: <xf:output value="serialize(/*)"/></xf:message>
        -->
    </xf:model>
  </xsl:template>
  
  <xsl:template name="FetchInstanceAndReplaceDiffgram">
    <xf:submission id="fetch-instance"
                    validate="false"
                    relevant="false"
                    method="get"
                    replace="instance"
                    serialize="none"
                    action="/services/{$dataset-name}Service.aspx">
      <xf:action ev:event="xforms-submit-done">
        <xf:message>Fetch Done</xf:message>
        <!-- insert this element as soon as possible, it will be needed for change-tracking
        NOTE: we must also insert this element AFTER replacing old contents with the new contents...
        -->
        <xf:insert
          nodeset="/diffgr:diffgram/mstns:{$dataset-name}"
          origin="instance('diffgram-templates')/diffgr:before">
        </xf:insert>
      </xf:action>
      <xf:message ev:event="xforms-submit-error" level="ephemeral">
        Fetch Failed!

        Write down the following information and report the problem:
        A submission error (<xf:output value="event('error-type')"/>) occurred.
        Response: <xf:output value="event('response-status-code')"/>.
        Reason given: <xf:output value="event('response-reason-phrase')"/>.
      </xf:message>
    </xf:submission>
  </xsl:template>

  <xsl:template name="SaveInstanceAndMergeDiffgrams">
    <xf:submission id="save-instance"
                    method="post"
                    replace="instance"
                    instance="instance-{$dataset-name}-changes"
                    ref="instance('instance-{$dataset-name}-changes')"
                    resource="/services/{$dataset-name}Service.aspx">
      <xf:action ev:event="xforms-submit-done">
        <xf:message level="ephemeral">Save Done</xf:message>

        <!--
        <xf:message>result diffgram: <xf:output value="serialize(instance('instance-{$dataset-name}-changes'))"/></xf:message>
        -->
              
        <!-- merge changes back to the main dataset -->

        <xsl:variable name="old">
          <xsl:text>/diffgr:diffgram/diffgr:before</xsl:text>
        </xsl:variable>
        <xsl:variable name="cur">
          <xsl:text>/diffgr:diffgram/mstns:</xsl:text>
          <xsl:value-of select="$dataset-name"/>
        </xsl:variable>
        <xsl:variable name="new">
          <xsl:text>instance('instance-</xsl:text>
          <xsl:value-of select="$dataset-name"/>
          <xsl:text>-changes')</xsl:text>
        </xsl:variable>
        <!-- another approach: accept all changes locally, then merge -->
        <xf:delete nodeset="{$old}/*"/>
        <xf:delete nodeset="{$cur}/*[@diffgr:hasChanges]"/>
        <!-- take care of autoincrement columns (or columns with defaults assigned by the database) -->
        <xf:insert context="{$cur}" nodeset="*" origin="{$new}/mstns:{$dataset-name}/*"/>
        <xf:delete nodeset="{$cur}/*[@diffgr:hasChanges='modified']"/>
        <!--
        bug: sometimes a record is duplicated, why?
        -->
        <xf:load resource="javascript:DS2XF_SortDataTable('instance-{$dataset-name}')"/>
        <!--
        <xf:message>
          result of merge:
          <xf:output value="serialize({$cur})"/>
        </xf:message>-->
      </xf:action>
      <xf:message ev:event="xforms-submit-error" level="ephemeral">
        Save Failed!

        Write down the following information and report the problem:
        A submission error (<xf:output value="event('error-type')"/>) occurred.
        Response: <xf:output value="event('response-status-code')"/>.
        Reason given: <xf:output value="event('response-reason-phrase')"/>.
      </xf:message>
    </xf:submission>
  </xsl:template>
</xsl:stylesheet>
