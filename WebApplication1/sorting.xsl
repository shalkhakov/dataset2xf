﻿<?xml version="1.0"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0"
  xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"
  xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">

  <!-- the sorting clause -->
  <!-- don't sort the diffgr:before element -->
  <xsl:template match="/diffgr:diffgram/*[namespace-uri() != 'urn:schemas-microsoft-com:xml-diffgram-v1']">
    <xsl:copy>
      <xsl:apply-templates select="child::node()">
        <xsl:sort data-type="number" order="ascending" select="@msdata:rowOrder"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

  <!-- the identity transform -->
  <xsl:template match="*|@*|text()">
    <xsl:copy>
      <xsl:apply-templates select="*|text()|@*"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
