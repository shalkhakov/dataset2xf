﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Schema;

namespace dataset2xsd_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // basic table
        public static void Example1() {
            var ds = new DataSet();

            ds.DataSetName = "myds";
            ds.Namespace = "http://myns.org/myns";

            var dt = new DataTable();
            dt.TableName = "Sample";
            dt.Namespace = ds.Namespace;

            dt.Columns.Add("Column1", typeof(int));
            dt.Columns.Add("Column2", typeof(string));
            dt.Columns.Add("Column3", typeof(object));
            dt.Columns.Add("Column4", typeof(float));
            dt.Columns.Add("Column5", typeof(bool));
            dt.Columns["Column1"].Caption = "Column 1";
            dt.Columns["Column1"].ReadOnly = true;
            dt.Columns["Column1"].AllowDBNull = false;
            dt.Columns["Column1"].AutoIncrement = true;
            dt.Columns["Column1"].AutoIncrementSeed = 1;
            dt.Columns["Column1"].AutoIncrementStep = 1;
            dt.Columns["Column2"].DefaultValue = "hello";
            dt.Columns["Column4"].DefaultValue = 3.5f;
            dt.Columns["Column5"].AllowDBNull = false;
            ds.Tables.Add(dt);

            ds.WriteXmlSchema(@"c:\temp\example1.xsd");
            /*
<?xml version="1.0" standalone="yes"?>
<xs:schema id="myds" targetNamespace="http://myns.org/myns" xmlns:mstns="http://myns.org/myns" xmlns="http://myns.org/myns" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" attributeFormDefault="qualified" elementFormDefault="qualified">
  <xs:element name="myds" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Sample">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Column1" msdata:ReadOnly="true" msdata:AutoIncrement="true" msdata:AutoIncrementSeed="1" msdata:Caption="Column 1" type="xs:int" />
              <xs:element name="Column2" type="xs:string" minOccurs="0" />
              <xs:element name="Column3" msdata:DataType="System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:anyType" minOccurs="0" />
              <xs:element name="Column4" type="xs:float" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
  </xs:element>
</xs:schema>
             */

            // add some data, but DON'T commit it
            // I'd like to know how changes are serialized
            var row = ds.Tables["Sample"].NewRow();
            row.SetField<int>(0, 1);
            row.SetField<string>(1, "hello");
            row.SetField<object>(2, (object)3);
            row.SetField<float>(3, 3.5f);
            row.SetField<bool>(4, false);
            ds.Tables["Sample"].Rows.Add(row);
            ds.AcceptChanges(); // let's say that this is the initial state
            /*
            var row2 = ds.Tables["Sample"].NewRow();
            row2.SetField<int>(0, 2);
            row2.SetField<string>(1, "wold");
            row2.SetField<object>(2, (object)3);
            row2.SetField<float>(3, 3.5f);
            row2.SetField<bool>(4, true);
            ds.Tables["Sample"].Rows.Add(row2);*/
            // edit the row
            ds.Tables["Sample"].Rows[0].SetField<string>(1, "world");
            // and delete it!
            ds.Tables["Sample"].Rows[0].Delete();
            // retrieve changes
            // see also: https://visualstudiomagazine.com/articles/2004/08/01/track-changes-with-xml-datasets.aspx
            var ds_diff = ds.GetChanges();
            if (ds_diff != null)
                ds_diff.WriteXml(@"c:\temp\example1-diff.xml", XmlWriteMode.DiffGram);
            // retrieve the whole data
            ds.WriteXml(@"c:\temp\example1.xml");
            ds.WriteXmlSchema(@"c:\temp\example1.xsd");
        }

        // table with a primary key
        public void Example2()
        {
            var ds = new DataSet();

            ds.DataSetName = "myds";
            ds.Namespace = "http://myns.org/myns";

            var dt = new DataTable();
            dt.TableName = "Sample";
            dt.Namespace = ds.Namespace;

            var col1 = new DataColumn("Column1", typeof(int));
            var col2 = new DataColumn("Column2", typeof(string));
            dt.Columns.Add(col1);
            dt.Columns.Add(col2);
            // this:
//           dt.PrimaryKey = new DataColumn[] { col1 };
            // is the same as this constraint:
            var pkCnstr = new UniqueConstraint("Constraint1", new DataColumn[]{ col1 }, true);
            dt.Constraints.Add(pkCnstr);
            ds.Tables.Add(dt);

            ds.WriteXmlSchema(@"c:\temp\example2.xsd");
            /*
<?xml version="1.0" standalone="yes"?>
<xs:schema id="myds" targetNamespace="http://myns.org/myns" xmlns:mstns="http://myns.org/myns" xmlns="http://myns.org/myns" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" attributeFormDefault="qualified" elementFormDefault="qualified">
  <xs:element name="myds" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Sample">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Column1" type="xs:int" />
              <xs:element name="Column2" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//mstns:Sample" />
      <xs:field xpath="mstns:Column1" />
    </xs:unique>
  </xs:element>
</xs:schema>
             */
        }

        // table with an additional constraint
        public void Example3()
        {
            var ds = new DataSet();

            ds.DataSetName = "myds";
            ds.Namespace = "http://myns.org/myns";

            var dt = new DataTable();
            dt.TableName = "Sample";
            dt.Namespace = ds.Namespace;

            var col1 = new DataColumn("Column1", typeof(int));
            var col2 = new DataColumn("Column2", typeof(string));
            dt.Columns.Add(col1);
            dt.Columns.Add(col2);
            // this:
            //           dt.PrimaryKey = new DataColumn[] { col1 };
            // is the same as this constraint:
            var pkCnstr = new UniqueConstraint("Constraint1", new DataColumn[] { col1 }, true);
            dt.Constraints.Add(pkCnstr);
            var col2Cnstr = new UniqueConstraint("Constraint2", new DataColumn[] { col2 });
            dt.Constraints.Add(col2Cnstr);
            ds.Tables.Add(dt);

            ds.WriteXmlSchema(@"c:\temp\example3.xsd");
            /*
             * same as example2, except that one additional constraint is rendered:
    <xs:unique name="Constraint2">
      <xs:selector xpath=".//mstns:Sample" />
      <xs:field xpath="mstns:Column2" />
    </xs:unique>
             */
        }

        // foreign key constraint
        public static void Example4()
        {
            var ds = new DataSet();

            ds.DataSetName = "myds";
            ds.Namespace = "http://myns.org/myns";

            var dt1 = new DataTable();
            {
                dt1.TableName = "Sample";
                dt1.Namespace = ds.Namespace;

                var col1 = new DataColumn("Column1", typeof(int));
                var col2 = new DataColumn("Column2", typeof(string));
                dt1.Columns.Add(col1);
                dt1.Columns.Add(col2);
                var pkCnstr = new UniqueConstraint("Constraint1", new DataColumn[] { col1 }, true);
                dt1.Constraints.Add(pkCnstr);
                var col2Cnstr = new UniqueConstraint("Constraint2", new DataColumn[] { col2 });
                dt1.Constraints.Add(col2Cnstr);
            }
            ds.Tables.Add(dt1);

            var dt2 = new DataTable();
            ds.Tables.Add(dt2);
            {
                dt2.TableName = "SampleUsage";
                dt2.Namespace = ds.Namespace;

                var col1 = new DataColumn("ID", typeof(int));
                var col2 = new DataColumn("Column", typeof(string));
                var col3 = new DataColumn("ID_Sample", typeof(int));
                dt2.Columns.Add(col1);
                dt2.Columns.Add(col2);
                dt2.Columns.Add(col3);

                var fk =
                    new ForeignKeyConstraint(new DataColumn[] { dt1.Columns["Column1"] }, new DataColumn[] { col3 });
                fk.ConstraintName = "FK";
                fk.AcceptRejectRule = AcceptRejectRule.None;
                fk.UpdateRule = Rule.None;
                fk.DeleteRule = Rule.None;
                dt2.Constraints.Add(fk);
            }

            ds.WriteXmlSchema(@"c:\temp\example4.xsd");
            /*
<?xml version="1.0" standalone="yes"?>
<xs:schema id="myds" targetNamespace="http://myns.org/myns" xmlns:mstns="http://myns.org/myns" xmlns="http://myns.org/myns" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" attributeFormDefault="qualified" elementFormDefault="qualified">
  <xs:element name="myds" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Sample">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Column1" type="xs:int" />
              <xs:element name="Column2" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="SampleUsage">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ID" type="xs:int" minOccurs="0" />
              <xs:element name="Column" type="xs:string" minOccurs="0" />
              <xs:element name="ID_Sample" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//mstns:Sample" />
      <xs:field xpath="mstns:Column1" />
    </xs:unique>
    <xs:unique name="Constraint2">
      <xs:selector xpath=".//mstns:Sample" />
      <xs:field xpath="mstns:Column2" />
    </xs:unique>
    <xs:keyref name="FK" refer="Constraint1" msdata:ConstraintOnly="true" msdata:UpdateRule="None" msdata:DeleteRule="None">
      <xs:selector xpath=".//mstns:SampleUsage" />
      <xs:field xpath="mstns:ID_Sample" />
    </xs:keyref>
  </xs:element>
</xs:schema>
             */
        }

        // custom data type
        public static void Example5()
        {
            var ds = new DataSet();

            ds.DataSetName = "myds";
            ds.Namespace = "http://myns.org/myns";

            var dt1 = new DataTable();
            {
                dt1.TableName = "Sample";
                dt1.Namespace = ds.Namespace;

                // it is actually OK to
                // save the geography as XML...
                var col1 = new DataColumn("Column1", typeof(int));
                var col2 = new DataColumn("Column2", typeof(System.Data.SqlTypes.SqlBinary));
                dt1.Columns.Add(col1);
                dt1.Columns.Add(col2);
                var pkCnstr = new UniqueConstraint("Constraint1", new DataColumn[] { col1 }, true);
                dt1.Constraints.Add(pkCnstr);
            }
            ds.Tables.Add(dt1);

            ds.WriteXmlSchema(@"c:\temp\example5.xsd");            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Example1();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Example2();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Example3();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Example4();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Example5();
        }
    }

    public sealed class TypeGeographySchemaImporterExtension : System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper
    {
        public TypeGeographySchemaImporterExtension() : base("geography", "System.Data.SqlTypes.SQLBinary", false) { }
    }
}
