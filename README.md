This project is aimed to generate editing forms based on XForms from
ADO.NET typed datasets.

Copyright notice:
- images are taken from the Dataphor project, dataphor.org
- code written by Artyom Shalkhakov is under LGPL v3
