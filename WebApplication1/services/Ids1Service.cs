﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.ServiceModel.Web;

namespace WebApplication1.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "Ids1Service" in both code and config file together.
    [ServiceContract]
    public interface Ids1Service
    {
        [OperationContract]
        [WebGet(ResponseFormat= WebMessageFormat.Xml, BodyStyle= WebMessageBodyStyle.Bare)]
        string Get();

        [OperationContract]
        [WebInvoke(Method="PUT", ResponseFormat= WebMessageFormat.Xml, RequestFormat= WebMessageFormat.Xml, BodyStyle= WebMessageBodyStyle.Bare)]
        string Update(string ds);
    }
}
