﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication1.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ds1Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ds1Service.svc or ds1Service.svc.cs at the Solution Explorer and start debugging.
    public class ds1Service : Ids1Service
    {
        public SqlConnection connection = new SqlConnection(@"Data Source=.;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;User Instance=True;");

        public string Get()
        {
            var adapter = new WebApplication1.datasets.ds1TableAdapters.SampleTableAdapter();

            var custDS = new datasets.ds1();
            custDS.SchemaSerializationMode = SchemaSerializationMode.ExcludeSchema;

            adapter.Fill(custDS.Sample);

            // don't send the diffgram
//            custDS.AcceptChanges();

            return custDS.GetXml();
        }

        public string Update(string custDS0)
        {
            var custDS = new datasets.ds1();
            var reader = new System.IO.StringReader(custDS0);
            custDS.ReadXml(reader);

            var adapter = new datasets.ds1TableAdapters.SampleTableAdapter();
            adapter.Connection = connection;
            adapter.Update(custDS);

            return custDS.GetXml();
        }
    }
}
