﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var svc = new services.ds1Service();

                var xsd = new datasets.ds1().GetXmlSchema();

                xformresult = GetHtml(Server.MapPath("~/dataset2xf.xslt"), xsd);
                Response.ContentType = "application/xml";
                Response.Write(xformresult);
            }
            catch
            {
                throw;
            }
        }
        string xformresult;

        public static string GetHtml(string xsltPath, string xmlPath)
        {
            var reader = new StringReader(xmlPath);
            XPathDocument document = new XPathDocument(System.Xml.XmlReader.Create(reader));
            // TODO: edit the mstns namespace in the [document]!
            // it should point precisely to the default namespace of xmlPath
            StringWriter writer = new StringWriter();
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(xsltPath);
            transform.Transform(document, null, writer);
            return writer.ToString();
        }
    }
}