﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1.services
{
    public partial class ds1Service1 : System.Web.UI.Page
    {
        private const string connectionString = @"Data Source=.;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;User Instance=True;";
        protected void Page_Load(object sender, EventArgs e)
        {
            Action<object, System.Data.SqlClient.SqlRowUpdatedEventArgs> handler = (object sender1, System.Data.SqlClient.SqlRowUpdatedEventArgs args) =>
            {
                if (args.StatementType == System.Data.StatementType.Insert)
                {
                    args.Status = System.Data.UpdateStatus.SkipCurrentRow;
                }
            };

            try
            {
                if (Request.HttpMethod == "GET")
                {
                    var ds = new datasets.ds1();
                    var adp = new datasets.ds1TableAdapters.SampleTableAdapter();
                    var adp2 = new datasets.ds1TableAdapters.AutoIncSampleTableAdapter();
                    var conn = new System.Data.SqlClient.SqlConnection(connectionString);
                    adp.Connection = conn;
                    adp2.Connection = conn;
                    
                    adp.Fill(ds.Sample);
                    adp2.Fill(ds.AutoIncSample);
                    // send the diffgram too!
                    ds.AcceptChanges();
                    Response.ContentType = "application/xml";
                    ds.WriteXml(Response.OutputStream, System.Data.XmlWriteMode.DiffGram);
//                    Response.Write(xml);
                }
                else if (Request.HttpMethod == "POST")
                {
                    var ds = new datasets.ds1();
                    /*
                    Response.ContentType = "text/html";
                    ds.ReadXml(Request.InputStream);
                    // just an echo
                    Response.Write("<html><head><title>foo</title></head><body><pre>");
                    ds.WriteXml(Response.OutputStream, System.Data.XmlWriteMode.DiffGram);
                    Response.Write("</pre></body></html>");
                    */

                    ds.ReadXml(Request.InputStream);
                    var adp = new datasets.ds1TableAdapters.SampleTableAdapter();
                    var adp2 = new datasets.ds1TableAdapters.AutoIncSampleTableAdapter();
                    adp2.Adapter.RowUpdated += new System.Data.SqlClient.SqlRowUpdatedEventHandler(handler);
                    try
                    {
                        var conn = new System.Data.SqlClient.SqlConnection(connectionString);
                        adp.Connection = conn;
                        adp2.Connection = conn;

                        adp.Update(ds.Sample);
                        adp2.Update(ds.AutoIncSample);
                    }
                    finally
                    {
                        adp2.Adapter.RowUpdated -= new System.Data.SqlClient.SqlRowUpdatedEventHandler(handler);
                    }

                    // well, if anything goes wrong, it should be put into the response, I suppose...
                    // that would be RESTful, right?
                    var xml = ds.GetXml();
                    ds.WriteXml(Response.OutputStream, System.Data.XmlWriteMode.DiffGram);
                }
            }
            catch
            {
                throw;
            }
        }
    }
}